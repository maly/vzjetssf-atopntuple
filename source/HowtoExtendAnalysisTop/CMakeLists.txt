# Auto-generated on: 2017-11-24 15:14:15.262411

# Declare the name of this package:
atlas_subdir( HowtoExtendAnalysisTop None )

# external dependencies
find_package( ROOT REQUIRED COMPONENTS Core Tree Hist RIO MathCore )
find_package( Boost )
find_package( lwtnn )

# This package depends on other packages:
atlas_depends_on_subdirs( PUBLIC
                          TopObjectSelectionTools
                          TopEventSelectionTools
                          TopEvent
                          TopConfiguration
                          TopAnalysis
                          JetAnalysisInterfaces
			  FlavorTagDiscriminants)

# This package uses ROOT:
find_package( ROOT REQUIRED COMPONENTS Core Gpad Tree Hist RIO MathCore Graf )

# Generate a CINT dictionary source file:
atlas_add_root_dictionary( HowtoExtendAnalysisTop _cintDictSource
                           ROOT_HEADERS Root/LinkDef.h
                           EXTERNAL_PACKAGES ROOT )

# Build a library that other components can link against:
atlas_add_library( HowtoExtendAnalysisTop Root/*.cxx Root/*.h Root/*.icc
                   HowtoExtendAnalysisTop/*.h HowtoExtendAnalysisTop/*.icc HowtoExtendAnalysisTop/*/*.h
                   HowtoExtendAnalysisTop/*/*.icc ${_cintDictSource} 
                   PUBLIC_HEADERS HowtoExtendAnalysisTop
                   LINK_LIBRARIES TopObjectSelectionTools
                                  TopEventSelectionTools
                                  TopEvent
                                  TopConfiguration
                                  TopAnalysis
                                  JetAnalysisInterfacesLib
                                  FlavorTagDiscriminants
                                  ${ROOT_LIBRARIES}
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
		   		${Boost_INCLUDE_DIRS}
 )

# Install data files from the package:
atlas_install_data( DIRECTORY share/* )
