LibraryNames libTopEventSelectionTools libTopEventReconstructionTools libHowtoExtendAnalysisTop

GRLDir  GoodRunsLists
GRLFile data15_13TeV/20170619/physics_25ns_21.0.19.xml data16_13TeV/20180129/physics_25ns_21.0.19.xml

PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.FS.v2/prw.merged.root
PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.AF.v2/prw.merged.root
PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root

ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMTopoJets
LargeJetCollectionName None
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName None
METCollectionName MET_Reference_AntiKt4EMTopo
TruthCollectionName TruthParticles
TruthJetCollectionName AntiKt4TruthJets

ObjectSelectionName top::ObjectLoaderStandardCuts # or the top::CustomObjectLoader defined in HowToExtendAnalysisTop
OutputFormat top::CustomEventSaver
OutputEvents SelectedEvents
OutputFilename output.root

ElectronID TightLH
ElectronIDLoose MediumLH
ElectronIsolation Gradient
ElectronIsolationLoose None

MuonQuality Medium
MuonQualityLoose Medium
MuonIsolation FCTight_FixedRad
MuonIsolationLoose None

# DoTight/DoLoose to activate the loose and tight trees
# each should be one in: Data, MC, Both, False
DoTight Both
DoLoose Data

# Turn on MetaData to pull IsAFII from metadata
UseAodMetaData True
# IsAFII False

NEvents 500


SELECTION customSelection
INITIAL
GRL
GOODCALO
PRIVTX
EVEN
RUN_NUMBER >= 297730
MU_N 25000 >= 1
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 >= 2
SAVE

