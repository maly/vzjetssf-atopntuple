

**Setup atlas and athena**

```
source setupATLAS
lsetup git

git clone https://gitlab.cern.ch/maly/vzjetssf-atopntuple.git

cd vzjetssf-atopntuple
```
**Make a non-delveloper setup of work area**
```
cd source/
asetup AnalysisBase,21.2.150,here
cd ..
mkdir build 
source full_compile.sh 
```
**Compile**
```
source setup.sh
source compile.sh
```

**Run locally**
```
cd run
top-xaod CUTFILE INPUTFILE
```
for example: `top-xaod cutfileforTest.txt inputforTest-zmm-sherpa221.txt`

**Run on Grid**
```
cd run/grid
./01SubmitToGrid.py
```
