#ifndef PLOTTING_FUNCS_HEADER
#define PLOTTING_FUNCS_HEADER

std::string      write_one_histo_to_file(TH1F* h);
std::string      write_Nhistos_to_file(std::vector<TH1F*> v_h, std::string OutputFileName);
void             save_histos_from_file(std::string fname_str, const char* xLabel="", const char* yLabel="", 
                    float xmin = -999,float xmax = -999, float ymin = -999, float ymax = -999, 
                    bool stacked = false, const char* stacked_hname = "", std::vector<std::string> legend_entries = {},
                    std::vector<int> stack_hColors = {}, std::string output_format = ".pdf");
std::vector<TH1F*> list_histos_inFile(std::string fname);   

#endif
