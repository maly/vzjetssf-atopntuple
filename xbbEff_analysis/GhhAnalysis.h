//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Feb 19 23:08:23 2021 by ROOT version 6.20/06
// from TTree nominal/tree
// found on file: outputTestDijetJZ0_MC16d_AllTaggers.root
//////////////////////////////////////////////////////////

#ifndef GhhAnalysis_h
#define GhhAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class GhhAnalysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          DSID;
   ULong64_t       EventNumber;
   UInt_t          LumiBlock;
   Float_t         PRWEventWeight;
   Float_t         EventWeight;
   Float_t         JVTEventWeight;
   Float_t         AverageInteractionsPerBunchCrossing;
   Float_t         ActualInteractionsPerBunchCrossing;
   vector<char>    *JetIsSelected;
   vector<char>    *JetIsSelectedLoose;
   vector<float>   *JetPt;
   vector<float>   *JetEta;
   vector<float>   *JetPhi;
   vector<float>   *JetMass;
   vector<float>   *JetHadronConeExclTruthLabelID;
   vector<char>    *TrackJetIsSelected;
   vector<float>   *TrackJetPt;
   vector<float>   *TrackJetEta;
   vector<float>   *TrackJetPhi;
   vector<float>   *TrackJetMass;
   vector<float>   *TrackJetHadronConeExclTruthLabelID;
   vector<char>    *TrackJetPassDR;
   vector<float>   *TrackJetDL1pb;
   vector<float>   *TrackJetDL1pc;
   vector<float>   *TrackJetDL1pu;
   vector<float>   *TrackJetDL1rpb;
   vector<float>   *TrackJetDL1rpc;
   vector<float>   *TrackJetDL1rpu;
   vector<float>   *TrackJetDL1rmupb;
   vector<float>   *TrackJetDL1rmupc;
   vector<float>   *TrackJetDL1rmupu;
   vector<vector<unsigned long> > *TrackjetAssociatedTrackParticles;
   vector<char>    *ElectronIsSelected;
   vector<char>    *ElectronIsSelectedLoose;
   vector<float>   *ElectronPt;
   vector<float>   *ElectronEta;
   vector<float>   *ElectronPhi;
   vector<float>   *ElectronCharge;
   vector<char>    *MuonIsSelected;
   vector<char>    *MuonIsSelectedLoose;
   vector<float>   *MuonPt;
   vector<float>   *MuonEta;
   vector<float>   *MuonPhi;
   vector<unsigned short> *MuonType;
   vector<unsigned char> *MuonQuality;
   vector<float>   *MuonCharge;
   vector<float>   *MuonEnergyLoss;
   vector<char>    *TauIsSelected;
   vector<char>    *TauIsSelectedLoose;
   vector<float>   *TauMass;
   vector<float>   *TauPt;
   vector<float>   *TauEta;
   vector<float>   *TauPhi;
   vector<float>   *TauCharge;
   vector<float>   *MetMet;
   vector<float>   *MetPhi;
   vector<char>    *LargeRJetIsSelected;
   vector<float>   *LargeRJetPt;
   vector<float>   *LargeRJetEta;
   vector<float>   *LargeRJetPhi;
   vector<float>   *LargeRJetMass;
   vector<float>   *LargeRJetCaloMass;
   vector<float>   *LargeRJetCaloPt;
   vector<float>   *LargeRJetCaloEta;
   vector<float>   *LargeRJetCaloPhi;
   vector<float>   *LargeRJetTAMass;
   vector<float>   *LargeRJetTAPt;
   vector<float>   *LargeRJetTAEta;
   vector<float>   *LargeRJetTAPhi;
   vector<float>   *LargeRJetTruthMass;
   vector<float>   *LargeRJetTruthPt;
   vector<float>   *LargeRJetTruthEta;
   vector<float>   *LargeRJetTruthPhi;
   vector<int>     *LargeRJetTruthLabel;
   vector<float>   *LargeRJetdR_W;
   vector<float>   *LargeRJetdR_Z;
   vector<float>   *LargeRJetdR_H;
   vector<float>   *LargeRJetdR_Top;
   vector<int>     *LargeRJetdR_NB;
   vector<float>   *LargeRJetXbbScoreTop;
   vector<float>   *LargeRJetXbbScoreQCD;
   vector<float>   *LargeRJetXbbScoreHiggs;
   vector<int>     *LargeRJetPassWNtrk_50;
   vector<int>     *LargeRJetPassWD2_50;
   vector<int>     *LargeRJetPassWMassLow_50;
   vector<int>     *LargeRJetPassWMassHigh_50;
   vector<int>     *LargeRJetPassZNtrk_50;
   vector<int>     *LargeRJetPassZD2_50;
   vector<int>     *LargeRJetPassZMassLow_50;
   vector<int>     *LargeRJetPassZMassHigh_50;
   vector<int>     *LargeRJetPassWNtrk_80;
   vector<int>     *LargeRJetPassWD2_80;
   vector<int>     *LargeRJetPassWMassLow_80;
   vector<int>     *LargeRJetPassWMassHigh_80;
   vector<int>     *LargeRJetPassZNtrk_80;
   vector<int>     *LargeRJetPassZD2_80;
   vector<int>     *LargeRJetPassZMassLow_80;
   vector<int>     *LargeRJetPassZMassHigh_80;
   vector<float>   *LargeRJetHbbScoreTop;
   vector<float>   *LargeRJetHbbScoreQCD;
   vector<float>   *LargeRJetHbbScoreHiggs;
   vector<float>   *LargeRJetHbbScoreTop_V2;
   vector<float>   *LargeRJetHbbScoreQCD_V2;
   vector<float>   *LargeRJetHbbScoreHiggs_V2;
   vector<float>   *LargeRJetNtrk;
   vector<float>   *LargeRJetNtrkPt500;
   vector<float>   *LargeRJetECF1;
   vector<float>   *LargeRJetECF2;
   vector<float>   *LargeRJetECF3;
   vector<float>   *LargeRJetD2;
   vector<float>   *LargeRJetTau1_wta;
   vector<float>   *LargeRJetTau2_wta;
   vector<float>   *LargeRJetTau3_wta;
   vector<float>   *LargeRJetTau21;
   vector<float>   *LargeRJetFoxWolfram2;
   vector<float>   *LargeRJetFoxWolfram1;
   vector<float>   *LargeRJetFoxWolfram0;
   vector<float>   *LargeRJetSplit12;
   vector<float>   *LargeRJetSplit23;
   vector<float>   *LargeRJetQw;
   vector<float>   *LargeRJetPlanarFlow;
   vector<float>   *LargeRJetAngularity;
   vector<float>   *LargeRJetAplanarity;
   vector<float>   *LargeRJetZCut12;
   vector<float>   *LargeRJetKtDR;
   vector<float>   *LargeRJetThrustMin;
   vector<float>   *LargeRJetThrustMaj;
   vector<float>   *LargeRJetSphericity;
   vector<float>   *LargeRJetMu12;
   vector<vector<unsigned long> > *LargeRJetAssociatedTrackJet;
   Bool_t          Analysis;

   // List of branches
   TBranch        *b_DSID;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_LumiBlock;   //!
   TBranch        *b_PRWEventWeight;   //!
   TBranch        *b_EventWeight;   //!
   TBranch        *b_JVTEventWeight;   //!
   TBranch        *b_AverageInteractionsPerBunchCrossing;   //!
   TBranch        *b_ActualInteractionsPerBunchCrossing;   //!
   TBranch        *b_JetIsSelected;   //!
   TBranch        *b_JetIsSelectedLoose;   //!
   TBranch        *b_JetPt;   //!
   TBranch        *b_JetEta;   //!
   TBranch        *b_JetPhi;   //!
   TBranch        *b_JetMass;   //!
   TBranch        *b_JetHadronConeExclTruthLabelID;   //!
   TBranch        *b_TrackJetIsSelected;   //!
   TBranch        *b_TrackJetPt;   //!
   TBranch        *b_TrackJetEta;   //!
   TBranch        *b_TrackJetPhi;   //!
   TBranch        *b_TrackJetMass;   //!
   TBranch        *b_TrackJetHadronConeExclTruthLabelID;   //!
   TBranch        *b_TrackJetPassDR;   //!
   TBranch        *b_TrackJetDL1pb;   //!
   TBranch        *b_TrackJetDL1pc;   //!
   TBranch        *b_TrackJetDL1pu;   //!
   TBranch        *b_TrackJetDL1rpb;   //!
   TBranch        *b_TrackJetDL1rpc;   //!
   TBranch        *b_TrackJetDL1rpu;   //!
   TBranch        *b_TrackJetDL1rmupb;   //!
   TBranch        *b_TrackJetDL1rmupc;   //!
   TBranch        *b_TrackJetDL1rmupu;   //!
   TBranch        *b_TrackjetAssociatedTrackParticles;   //!
   TBranch        *b_ElectronIsSelected;   //!
   TBranch        *b_ElectronIsSelectedLoose;   //!
   TBranch        *b_ElectronPt;   //!
   TBranch        *b_ElectronEta;   //!
   TBranch        *b_ElectronPhi;   //!
   TBranch        *b_ElectronCharge;   //!
   TBranch        *b_MuonIsSelected;   //!
   TBranch        *b_MuonIsSelectedLoose;   //!
   TBranch        *b_MuonPt;   //!
   TBranch        *b_MuonEta;   //!
   TBranch        *b_MuonPhi;   //!
   TBranch        *b_MuonType;   //!
   TBranch        *b_MuonQuality;   //!
   TBranch        *b_MuonCharge;   //!
   TBranch        *b_MuonEnergyLoss;   //!
   TBranch        *b_TauIsSelected;   //!
   TBranch        *b_TauIsSelectedLoose;   //!
   TBranch        *b_TauMass;   //!
   TBranch        *b_TauPt;   //!
   TBranch        *b_TauEta;   //!
   TBranch        *b_TauPhi;   //!
   TBranch        *b_TauCharge;   //!
   TBranch        *b_MetMet;   //!
   TBranch        *b_MetPhi;   //!
   TBranch        *b_LargeRJetIsSelected;   //!
   TBranch        *b_LargeRJetPt;   //!
   TBranch        *b_LargeRJetEta;   //!
   TBranch        *b_LargeRJetPhi;   //!
   TBranch        *b_LargeRJetMass;   //!
   TBranch        *b_LargeRJetCaloMass;   //!
   TBranch        *b_LargeRJetCaloPt;   //!
   TBranch        *b_LargeRJetCaloEta;   //!
   TBranch        *b_LargeRJetCaloPhi;   //!
   TBranch        *b_LargeRJetTAMass;   //!
   TBranch        *b_LargeRJetTAPt;   //!
   TBranch        *b_LargeRJetTAEta;   //!
   TBranch        *b_LargeRJetTAPhi;   //!
   TBranch        *b_LargeRJetTruthMass;   //!
   TBranch        *b_LargeRJetTruthPt;   //!
   TBranch        *b_LargeRJetTruthEta;   //!
   TBranch        *b_LargeRJetTruthPhi;   //!
   TBranch        *b_LargeRJetTruthLabel;   //!
   TBranch        *b_LargeRJetdR_W;   //!
   TBranch        *b_LargeRJetdR_Z;   //!
   TBranch        *b_LargeRJetdR_H;   //!
   TBranch        *b_LargeRJetdR_Top;   //!
   TBranch        *b_LargeRJetdR_NB;   //!
   TBranch        *b_LargeRJetXbbScoreTop;   //!
   TBranch        *b_LargeRJetXbbScoreQCD;   //!
   TBranch        *b_LargeRJetXbbScoreHiggs;   //!
   TBranch        *b_LargeRJetPassWNtrk_50;   //!
   TBranch        *b_LargeRJetPassWD2_50;   //!
   TBranch        *b_LargeRJetPassWMassLow_50;   //!
   TBranch        *b_LargeRJetPassWMassHigh_50;   //!
   TBranch        *b_LargeRJetPassZNtrk_50;   //!
   TBranch        *b_LargeRJetPassZD2_50;   //!
   TBranch        *b_LargeRJetPassZMassLow_50;   //!
   TBranch        *b_LargeRJetPassZMassHigh_50;   //!
   TBranch        *b_LargeRJetPassWNtrk_80;   //!
   TBranch        *b_LargeRJetPassWD2_80;   //!
   TBranch        *b_LargeRJetPassWMassLow_80;   //!
   TBranch        *b_LargeRJetPassWMassHigh_80;   //!
   TBranch        *b_LargeRJetPassZNtrk_80;   //!
   TBranch        *b_LargeRJetPassZD2_80;   //!
   TBranch        *b_LargeRJetPassZMassLow_80;   //!
   TBranch        *b_LargeRJetPassZMassHigh_80;   //!
   TBranch        *b_LargeRJetHbbScoreTop;   //!
   TBranch        *b_LargeRJetHbbScoreQCD;   //!
   TBranch        *b_LargeRJetHbbScoreHiggs;   //!
   TBranch        *b_LargeRJetHbbScoreTop_V2;   //!
   TBranch        *b_LargeRJetHbbScoreQCD_V2;   //!
   TBranch        *b_LargeRJetHbbScoreHiggs_V2;   //!
   TBranch        *b_LargeRJetNtrk;   //!
   TBranch        *b_LargeRJetNtrkPt500;   //!
   TBranch        *b_LargeRJetECF1;   //!
   TBranch        *b_LargeRJetECF2;   //!
   TBranch        *b_LargeRJetECF3;   //!
   TBranch        *b_LargeRJetD2;   //!
   TBranch        *b_LargeRJetTau1_wta;   //!
   TBranch        *b_LargeRJetTau2_wta;   //!
   TBranch        *b_LargeRJetTau3_wta;   //!
   TBranch        *b_LargeRJetTau21;   //!
   TBranch        *b_LargeRJetFoxWolfram2;   //!
   TBranch        *b_LargeRJetFoxWolfram1;   //!
   TBranch        *b_LargeRJetFoxWolfram0;   //!
   TBranch        *b_LargeRJetSplit12;   //!
   TBranch        *b_LargeRJetSplit23;   //!
   TBranch        *b_LargeRJetQw;   //!
   TBranch        *b_LargeRJetPlanarFlow;   //!
   TBranch        *b_LargeRJetAngularity;   //!
   TBranch        *b_LargeRJetAplanarity;   //!
   TBranch        *b_LargeRJetZCut12;   //!
   TBranch        *b_LargeRJetKtDR;   //!
   TBranch        *b_LargeRJetThrustMin;   //!
   TBranch        *b_LargeRJetThrustMaj;   //!
   TBranch        *b_LargeRJetSphericity;   //!
   TBranch        *b_LargeRJetMu12;   //!
   TBranch        *b_LargeRJetAssociatedTrackJet;   //!
   TBranch        *b_Analysis;   //!

   GhhAnalysis(TTree *tree=0);
   virtual ~GhhAnalysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

};

#endif

#ifdef GhhAnalysis_cxx
GhhAnalysis::GhhAnalysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("outputTestGbbbbMC16dFTAG1_AllTaggers.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("outputTestGbbbbMC16dFTAG1_AllTaggers.root");
      }
      f->GetObject("nominal",tree);

   }
   Init(tree);
}

GhhAnalysis::~GhhAnalysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t GhhAnalysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t GhhAnalysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void GhhAnalysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   JetIsSelected = 0;
   JetIsSelectedLoose = 0;
   JetPt = 0;
   JetEta = 0;
   JetPhi = 0;
   JetMass = 0;
   JetHadronConeExclTruthLabelID = 0;
   TrackJetIsSelected = 0;
   TrackJetPt = 0;
   TrackJetEta = 0;
   TrackJetPhi = 0;
   TrackJetMass = 0;
   TrackJetHadronConeExclTruthLabelID = 0;
   TrackJetPassDR = 0;
   TrackJetDL1pb = 0;
   TrackJetDL1pc = 0;
   TrackJetDL1pu = 0;
   TrackJetDL1rpb = 0;
   TrackJetDL1rpc = 0;
   TrackJetDL1rpu = 0;
   TrackJetDL1rmupb = 0;
   TrackJetDL1rmupc = 0;
   TrackJetDL1rmupu = 0;
   TrackjetAssociatedTrackParticles = 0;
   ElectronIsSelected = 0;
   ElectronIsSelectedLoose = 0;
   ElectronPt = 0;
   ElectronEta = 0;
   ElectronPhi = 0;
   ElectronCharge = 0;
   MuonIsSelected = 0;
   MuonIsSelectedLoose = 0;
   MuonPt = 0;
   MuonEta = 0;
   MuonPhi = 0;
   MuonType = 0;
   MuonQuality = 0;
   MuonCharge = 0;
   MuonEnergyLoss = 0;
   TauIsSelected = 0;
   TauIsSelectedLoose = 0;
   TauMass = 0;
   TauPt = 0;
   TauEta = 0;
   TauPhi = 0;
   TauCharge = 0;
   MetMet = 0;
   MetPhi = 0;
   LargeRJetIsSelected = 0;
   LargeRJetPt = 0;
   LargeRJetEta = 0;
   LargeRJetPhi = 0;
   LargeRJetMass = 0;
   LargeRJetCaloMass = 0;
   LargeRJetCaloPt = 0;
   LargeRJetCaloEta = 0;
   LargeRJetCaloPhi = 0;
   LargeRJetTAMass = 0;
   LargeRJetTAPt = 0;
   LargeRJetTAEta = 0;
   LargeRJetTAPhi = 0;
   LargeRJetTruthMass = 0;
   LargeRJetTruthPt = 0;
   LargeRJetTruthEta = 0;
   LargeRJetTruthPhi = 0;
   LargeRJetTruthLabel = 0;
   LargeRJetdR_W = 0;
   LargeRJetdR_Z = 0;
   LargeRJetdR_H = 0;
   LargeRJetdR_Top = 0;
   LargeRJetdR_NB = 0;
   LargeRJetXbbScoreTop = 0;
   LargeRJetXbbScoreQCD = 0;
   LargeRJetXbbScoreHiggs = 0;
   LargeRJetPassWNtrk_50 = 0;
   LargeRJetPassWD2_50 = 0;
   LargeRJetPassWMassLow_50 = 0;
   LargeRJetPassWMassHigh_50 = 0;
   LargeRJetPassZNtrk_50 = 0;
   LargeRJetPassZD2_50 = 0;
   LargeRJetPassZMassLow_50 = 0;
   LargeRJetPassZMassHigh_50 = 0;
   LargeRJetPassWNtrk_80 = 0;
   LargeRJetPassWD2_80 = 0;
   LargeRJetPassWMassLow_80 = 0;
   LargeRJetPassWMassHigh_80 = 0;
   LargeRJetPassZNtrk_80 = 0;
   LargeRJetPassZD2_80 = 0;
   LargeRJetPassZMassLow_80 = 0;
   LargeRJetPassZMassHigh_80 = 0;
   LargeRJetHbbScoreTop = 0;
   LargeRJetHbbScoreQCD = 0;
   LargeRJetHbbScoreHiggs = 0;
   LargeRJetHbbScoreTop_V2 = 0;
   LargeRJetHbbScoreQCD_V2 = 0;
   LargeRJetHbbScoreHiggs_V2 = 0;
   LargeRJetNtrk = 0;
   LargeRJetNtrkPt500 = 0;
   LargeRJetECF1 = 0;
   LargeRJetECF2 = 0;
   LargeRJetECF3 = 0;
   LargeRJetD2 = 0;
   LargeRJetTau1_wta = 0;
   LargeRJetTau2_wta = 0;
   LargeRJetTau3_wta = 0;
   LargeRJetTau21 = 0;
   LargeRJetFoxWolfram2 = 0;
   LargeRJetFoxWolfram1 = 0;
   LargeRJetFoxWolfram0 = 0;
   LargeRJetSplit12 = 0;
   LargeRJetSplit23 = 0;
   LargeRJetQw = 0;
   LargeRJetPlanarFlow = 0;
   LargeRJetAngularity = 0;
   LargeRJetAplanarity = 0;
   LargeRJetZCut12 = 0;
   LargeRJetKtDR = 0;
   LargeRJetThrustMin = 0;
   LargeRJetThrustMaj = 0;
   LargeRJetSphericity = 0;
   LargeRJetMu12 = 0;
   LargeRJetAssociatedTrackJet = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("DSID", &DSID, &b_DSID);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("LumiBlock", &LumiBlock, &b_LumiBlock);
   fChain->SetBranchAddress("PRWEventWeight", &PRWEventWeight, &b_PRWEventWeight);
   fChain->SetBranchAddress("EventWeight", &EventWeight, &b_EventWeight);
   fChain->SetBranchAddress("JVTEventWeight", &JVTEventWeight, &b_JVTEventWeight);
   fChain->SetBranchAddress("AverageInteractionsPerBunchCrossing", &AverageInteractionsPerBunchCrossing, &b_AverageInteractionsPerBunchCrossing);
   fChain->SetBranchAddress("ActualInteractionsPerBunchCrossing", &ActualInteractionsPerBunchCrossing, &b_ActualInteractionsPerBunchCrossing);
   fChain->SetBranchAddress("JetIsSelected", &JetIsSelected, &b_JetIsSelected);
   fChain->SetBranchAddress("JetIsSelectedLoose", &JetIsSelectedLoose, &b_JetIsSelectedLoose);
   fChain->SetBranchAddress("JetPt", &JetPt, &b_JetPt);
   fChain->SetBranchAddress("JetEta", &JetEta, &b_JetEta);
   fChain->SetBranchAddress("JetPhi", &JetPhi, &b_JetPhi);
   fChain->SetBranchAddress("JetMass", &JetMass, &b_JetMass);
   fChain->SetBranchAddress("JetHadronConeExclTruthLabelID", &JetHadronConeExclTruthLabelID, &b_JetHadronConeExclTruthLabelID);
   fChain->SetBranchAddress("TrackJetIsSelected", &TrackJetIsSelected, &b_TrackJetIsSelected);
   fChain->SetBranchAddress("TrackJetPt", &TrackJetPt, &b_TrackJetPt);
   fChain->SetBranchAddress("TrackJetEta", &TrackJetEta, &b_TrackJetEta);
   fChain->SetBranchAddress("TrackJetPhi", &TrackJetPhi, &b_TrackJetPhi);
   fChain->SetBranchAddress("TrackJetMass", &TrackJetMass, &b_TrackJetMass);
   fChain->SetBranchAddress("TrackJetHadronConeExclTruthLabelID", &TrackJetHadronConeExclTruthLabelID, &b_TrackJetHadronConeExclTruthLabelID);
   fChain->SetBranchAddress("TrackJetPassDR", &TrackJetPassDR, &b_TrackJetPassDR);
   fChain->SetBranchAddress("TrackJetDL1pb", &TrackJetDL1pb, &b_TrackJetDL1pb);
   fChain->SetBranchAddress("TrackJetDL1pc", &TrackJetDL1pc, &b_TrackJetDL1pc);
   fChain->SetBranchAddress("TrackJetDL1pu", &TrackJetDL1pu, &b_TrackJetDL1pu);
   fChain->SetBranchAddress("TrackJetDL1rpb", &TrackJetDL1rpb, &b_TrackJetDL1rpb);
   fChain->SetBranchAddress("TrackJetDL1rpc", &TrackJetDL1rpc, &b_TrackJetDL1rpc);
   fChain->SetBranchAddress("TrackJetDL1rpu", &TrackJetDL1rpu, &b_TrackJetDL1rpu);
   fChain->SetBranchAddress("TrackJetDL1rmupb", &TrackJetDL1rmupb, &b_TrackJetDL1rmupb);
   fChain->SetBranchAddress("TrackJetDL1rmupc", &TrackJetDL1rmupc, &b_TrackJetDL1rmupc);
   fChain->SetBranchAddress("TrackJetDL1rmupu", &TrackJetDL1rmupu, &b_TrackJetDL1rmupu);
   fChain->SetBranchAddress("TrackjetAssociatedTrackParticles", &TrackjetAssociatedTrackParticles, &b_TrackjetAssociatedTrackParticles);
   fChain->SetBranchAddress("ElectronIsSelected", &ElectronIsSelected, &b_ElectronIsSelected);
   fChain->SetBranchAddress("ElectronIsSelectedLoose", &ElectronIsSelectedLoose, &b_ElectronIsSelectedLoose);
   fChain->SetBranchAddress("ElectronPt", &ElectronPt, &b_ElectronPt);
   fChain->SetBranchAddress("ElectronEta", &ElectronEta, &b_ElectronEta);
   fChain->SetBranchAddress("ElectronPhi", &ElectronPhi, &b_ElectronPhi);
   fChain->SetBranchAddress("ElectronCharge", &ElectronCharge, &b_ElectronCharge);
   fChain->SetBranchAddress("MuonIsSelected", &MuonIsSelected, &b_MuonIsSelected);
   fChain->SetBranchAddress("MuonIsSelectedLoose", &MuonIsSelectedLoose, &b_MuonIsSelectedLoose);
   fChain->SetBranchAddress("MuonPt", &MuonPt, &b_MuonPt);
   fChain->SetBranchAddress("MuonEta", &MuonEta, &b_MuonEta);
   fChain->SetBranchAddress("MuonPhi", &MuonPhi, &b_MuonPhi);
   fChain->SetBranchAddress("MuonType", &MuonType, &b_MuonType);
   fChain->SetBranchAddress("MuonQuality", &MuonQuality, &b_MuonQuality);
   fChain->SetBranchAddress("MuonCharge", &MuonCharge, &b_MuonCharge);
   fChain->SetBranchAddress("MuonEnergyLoss", &MuonEnergyLoss, &b_MuonEnergyLoss);
   fChain->SetBranchAddress("TauIsSelected", &TauIsSelected, &b_TauIsSelected);
   fChain->SetBranchAddress("TauIsSelectedLoose", &TauIsSelectedLoose, &b_TauIsSelectedLoose);
   fChain->SetBranchAddress("TauMass", &TauMass, &b_TauMass);
   fChain->SetBranchAddress("TauPt", &TauPt, &b_TauPt);
   fChain->SetBranchAddress("TauEta", &TauEta, &b_TauEta);
   fChain->SetBranchAddress("TauPhi", &TauPhi, &b_TauPhi);
   fChain->SetBranchAddress("TauCharge", &TauCharge, &b_TauCharge);
   fChain->SetBranchAddress("MetMet", &MetMet, &b_MetMet);
   fChain->SetBranchAddress("MetPhi", &MetPhi, &b_MetPhi);
   fChain->SetBranchAddress("LargeRJetIsSelected", &LargeRJetIsSelected, &b_LargeRJetIsSelected);
   fChain->SetBranchAddress("LargeRJetPt", &LargeRJetPt, &b_LargeRJetPt);
   fChain->SetBranchAddress("LargeRJetEta", &LargeRJetEta, &b_LargeRJetEta);
   fChain->SetBranchAddress("LargeRJetPhi", &LargeRJetPhi, &b_LargeRJetPhi);
   fChain->SetBranchAddress("LargeRJetMass", &LargeRJetMass, &b_LargeRJetMass);
   fChain->SetBranchAddress("LargeRJetCaloMass", &LargeRJetCaloMass, &b_LargeRJetCaloMass);
   fChain->SetBranchAddress("LargeRJetCaloPt", &LargeRJetCaloPt, &b_LargeRJetCaloPt);
   fChain->SetBranchAddress("LargeRJetCaloEta", &LargeRJetCaloEta, &b_LargeRJetCaloEta);
   fChain->SetBranchAddress("LargeRJetCaloPhi", &LargeRJetCaloPhi, &b_LargeRJetCaloPhi);
   fChain->SetBranchAddress("LargeRJetTAMass", &LargeRJetTAMass, &b_LargeRJetTAMass);
   fChain->SetBranchAddress("LargeRJetTAPt", &LargeRJetTAPt, &b_LargeRJetTAPt);
   fChain->SetBranchAddress("LargeRJetTAEta", &LargeRJetTAEta, &b_LargeRJetTAEta);
   fChain->SetBranchAddress("LargeRJetTAPhi", &LargeRJetTAPhi, &b_LargeRJetTAPhi);
   fChain->SetBranchAddress("LargeRJetTruthMass", &LargeRJetTruthMass, &b_LargeRJetTruthMass);
   fChain->SetBranchAddress("LargeRJetTruthPt", &LargeRJetTruthPt, &b_LargeRJetTruthPt);
   fChain->SetBranchAddress("LargeRJetTruthEta", &LargeRJetTruthEta, &b_LargeRJetTruthEta);
   fChain->SetBranchAddress("LargeRJetTruthPhi", &LargeRJetTruthPhi, &b_LargeRJetTruthPhi);
   fChain->SetBranchAddress("LargeRJetTruthLabel", &LargeRJetTruthLabel, &b_LargeRJetTruthLabel);
   fChain->SetBranchAddress("LargeRJetdR_W", &LargeRJetdR_W, &b_LargeRJetdR_W);
   fChain->SetBranchAddress("LargeRJetdR_Z", &LargeRJetdR_Z, &b_LargeRJetdR_Z);
   fChain->SetBranchAddress("LargeRJetdR_H", &LargeRJetdR_H, &b_LargeRJetdR_H);
   fChain->SetBranchAddress("LargeRJetdR_Top", &LargeRJetdR_Top, &b_LargeRJetdR_Top);
   fChain->SetBranchAddress("LargeRJetdR_NB", &LargeRJetdR_NB, &b_LargeRJetdR_NB);
   fChain->SetBranchAddress("LargeRJetXbbScoreTop", &LargeRJetXbbScoreTop, &b_LargeRJetXbbScoreTop);
   fChain->SetBranchAddress("LargeRJetXbbScoreQCD", &LargeRJetXbbScoreQCD, &b_LargeRJetXbbScoreQCD);
   fChain->SetBranchAddress("LargeRJetXbbScoreHiggs", &LargeRJetXbbScoreHiggs, &b_LargeRJetXbbScoreHiggs);
   fChain->SetBranchAddress("LargeRJetPassWNtrk_50", &LargeRJetPassWNtrk_50, &b_LargeRJetPassWNtrk_50);
   fChain->SetBranchAddress("LargeRJetPassWD2_50", &LargeRJetPassWD2_50, &b_LargeRJetPassWD2_50);
   fChain->SetBranchAddress("LargeRJetPassWMassLow_50", &LargeRJetPassWMassLow_50, &b_LargeRJetPassWMassLow_50);
   fChain->SetBranchAddress("LargeRJetPassWMassHigh_50", &LargeRJetPassWMassHigh_50, &b_LargeRJetPassWMassHigh_50);
   fChain->SetBranchAddress("LargeRJetPassZNtrk_50", &LargeRJetPassZNtrk_50, &b_LargeRJetPassZNtrk_50);
   fChain->SetBranchAddress("LargeRJetPassZD2_50", &LargeRJetPassZD2_50, &b_LargeRJetPassZD2_50);
   fChain->SetBranchAddress("LargeRJetPassZMassLow_50", &LargeRJetPassZMassLow_50, &b_LargeRJetPassZMassLow_50);
   fChain->SetBranchAddress("LargeRJetPassZMassHigh_50", &LargeRJetPassZMassHigh_50, &b_LargeRJetPassZMassHigh_50);
   fChain->SetBranchAddress("LargeRJetPassWNtrk_80", &LargeRJetPassWNtrk_80, &b_LargeRJetPassWNtrk_80);
   fChain->SetBranchAddress("LargeRJetPassWD2_80", &LargeRJetPassWD2_80, &b_LargeRJetPassWD2_80);
   fChain->SetBranchAddress("LargeRJetPassWMassLow_80", &LargeRJetPassWMassLow_80, &b_LargeRJetPassWMassLow_80);
   fChain->SetBranchAddress("LargeRJetPassWMassHigh_80", &LargeRJetPassWMassHigh_80, &b_LargeRJetPassWMassHigh_80);
   fChain->SetBranchAddress("LargeRJetPassZNtrk_80", &LargeRJetPassZNtrk_80, &b_LargeRJetPassZNtrk_80);
   fChain->SetBranchAddress("LargeRJetPassZD2_80", &LargeRJetPassZD2_80, &b_LargeRJetPassZD2_80);
   fChain->SetBranchAddress("LargeRJetPassZMassLow_80", &LargeRJetPassZMassLow_80, &b_LargeRJetPassZMassLow_80);
   fChain->SetBranchAddress("LargeRJetPassZMassHigh_80", &LargeRJetPassZMassHigh_80, &b_LargeRJetPassZMassHigh_80);
   fChain->SetBranchAddress("LargeRJetHbbScoreTop", &LargeRJetHbbScoreTop, &b_LargeRJetHbbScoreTop);
   fChain->SetBranchAddress("LargeRJetHbbScoreQCD", &LargeRJetHbbScoreQCD, &b_LargeRJetHbbScoreQCD);
   fChain->SetBranchAddress("LargeRJetHbbScoreHiggs", &LargeRJetHbbScoreHiggs, &b_LargeRJetHbbScoreHiggs);
   fChain->SetBranchAddress("LargeRJetHbbScoreTop_V2", &LargeRJetHbbScoreTop_V2, &b_LargeRJetHbbScoreTop_V2);
   fChain->SetBranchAddress("LargeRJetHbbScoreQCD_V2", &LargeRJetHbbScoreQCD_V2, &b_LargeRJetHbbScoreQCD_V2);
   fChain->SetBranchAddress("LargeRJetHbbScoreHiggs_V2", &LargeRJetHbbScoreHiggs_V2, &b_LargeRJetHbbScoreHiggs_V2);
   fChain->SetBranchAddress("LargeRJetNtrk", &LargeRJetNtrk, &b_LargeRJetNtrk);
   fChain->SetBranchAddress("LargeRJetNtrkPt500", &LargeRJetNtrkPt500, &b_LargeRJetNtrkPt500);
   fChain->SetBranchAddress("LargeRJetECF1", &LargeRJetECF1, &b_LargeRJetECF1);
   fChain->SetBranchAddress("LargeRJetECF2", &LargeRJetECF2, &b_LargeRJetECF2);
   fChain->SetBranchAddress("LargeRJetECF3", &LargeRJetECF3, &b_LargeRJetECF3);
   fChain->SetBranchAddress("LargeRJetD2", &LargeRJetD2, &b_LargeRJetD2);
   fChain->SetBranchAddress("LargeRJetTau1_wta", &LargeRJetTau1_wta, &b_LargeRJetTau1_wta);
   fChain->SetBranchAddress("LargeRJetTau2_wta", &LargeRJetTau2_wta, &b_LargeRJetTau2_wta);
   fChain->SetBranchAddress("LargeRJetTau3_wta", &LargeRJetTau3_wta, &b_LargeRJetTau3_wta);
   fChain->SetBranchAddress("LargeRJetTau21", &LargeRJetTau21, &b_LargeRJetTau21);
   fChain->SetBranchAddress("LargeRJetFoxWolfram2", &LargeRJetFoxWolfram2, &b_LargeRJetFoxWolfram2);
   fChain->SetBranchAddress("LargeRJetFoxWolfram1", &LargeRJetFoxWolfram1, &b_LargeRJetFoxWolfram1);
   fChain->SetBranchAddress("LargeRJetFoxWolfram0", &LargeRJetFoxWolfram0, &b_LargeRJetFoxWolfram0);
   fChain->SetBranchAddress("LargeRJetSplit12", &LargeRJetSplit12, &b_LargeRJetSplit12);
   fChain->SetBranchAddress("LargeRJetSplit23", &LargeRJetSplit23, &b_LargeRJetSplit23);
   fChain->SetBranchAddress("LargeRJetQw", &LargeRJetQw, &b_LargeRJetQw);
   fChain->SetBranchAddress("LargeRJetPlanarFlow", &LargeRJetPlanarFlow, &b_LargeRJetPlanarFlow);
   fChain->SetBranchAddress("LargeRJetAngularity", &LargeRJetAngularity, &b_LargeRJetAngularity);
   fChain->SetBranchAddress("LargeRJetAplanarity", &LargeRJetAplanarity, &b_LargeRJetAplanarity);
   fChain->SetBranchAddress("LargeRJetZCut12", &LargeRJetZCut12, &b_LargeRJetZCut12);
   fChain->SetBranchAddress("LargeRJetKtDR", &LargeRJetKtDR, &b_LargeRJetKtDR);
   fChain->SetBranchAddress("LargeRJetThrustMin", &LargeRJetThrustMin, &b_LargeRJetThrustMin);
   fChain->SetBranchAddress("LargeRJetThrustMaj", &LargeRJetThrustMaj, &b_LargeRJetThrustMaj);
   fChain->SetBranchAddress("LargeRJetSphericity", &LargeRJetSphericity, &b_LargeRJetSphericity);
   fChain->SetBranchAddress("LargeRJetMu12", &LargeRJetMu12, &b_LargeRJetMu12);
   fChain->SetBranchAddress("LargeRJetAssociatedTrackJet", &LargeRJetAssociatedTrackJet, &b_LargeRJetAssociatedTrackJet);
   fChain->SetBranchAddress("Analysis", &Analysis, &b_Analysis);
   Notify();
}

Bool_t GhhAnalysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void GhhAnalysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t GhhAnalysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef GhhAnalysis_cxx
