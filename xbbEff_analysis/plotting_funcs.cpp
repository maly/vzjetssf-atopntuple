std::string write_one_histo_to_file(TH1F* h){
  std::string hname_str(h->GetName());
  std::string file_ext_str(".root");
  std::string file_name_str = hname_str+file_ext_str;
  const char *file_name_char = file_name_str.c_str();
  //TFile *my_file = new TFile(file_name_char,"RECREATE");
  TFile* my_file = TFile::Open(file_name_char, "recreate");

  //Store the clone, to maintain ownership of original histogram to this class not the file we are opening 
  TH1F* hClone  = (TH1F*) h->Clone(h->GetName());
  hClone->SetDirectory(my_file);
  hClone->Write();
  my_file->Close();

  return file_name_str ;
}

std::string write_Nhistos_to_file(std::vector<TH1F*> v_h, std::string OutputFileName){

  std::string file_ext_str(".root");
  std::string file_name_str = OutputFileName+file_ext_str;
  const char *file_name_char = file_name_str.c_str();
  TFile* my_file = TFile::Open(file_name_char, "recreate");
  for(int i = 0; i < v_h.size(); i++){
     //Store the clone, to maintain ownership of original histogram to this class not the file we are opening 
     TH1F* hClone  = (TH1F*) v_h.at(i)->Clone(v_h.at(i)->GetName());
     hClone->SetDirectory(nullptr);
     hClone->SetDirectory(my_file);
     hClone->Write();
  }
  my_file->Close();

  return file_name_str;
}

void save_histos_from_file(std::string fname_str, const char* xLabel="", const char* yLabel="", 
   float xmin = -999,float xmax = -999, float ymin = -999, float ymax = -999, 
   bool stacked = false, const char* stacked_hname = "", std::vector<std::string> legend_entries = {},
  std::vector<int> stack_hColors = {}, std::string output_format = ".pdf"){
  

  //=============================== Get File Ready ========================================================//
    //Open the file 
  const char* file_name_char = fname_str.c_str();
    TFile* my_file = TFile::Open(file_name_char, "READ");
  //Check file is valid 
  if (my_file->IsZombie()) { cout << "Unable to open " << file_name_char << " for reading..." <<endl; exit(0); }
  //======================================================================================================//
  //=============================== Get the histograms in file ========================================================//
  std::vector<TH1F*> histos_infile = list_histos_inFile(file_name_char);
  //Make sure the histograms don't belong to anyone anymore 
  for(int i = 0; i < histos_infile.size(); i++){
  TH1F* h = (TH1F*) histos_infile.at(i);
  h->SetDirectory(0);
  }
  //Close the file now
  my_file->Close();
  //=============================== One Individual Histogram per PDF ========================================================//
  if(!stacked){
    for(int i = 0; i < histos_infile.size(); i++){

      TH1F* h = (TH1F*) histos_infile.at(i);
      std::string hname(h->GetName());
      std::string OutFileName_str = hname+output_format;
      const char* OutFileName_char= OutFileName_str.c_str();
      TCanvas *c = new TCanvas("c","c");
      c->cd();
      float x_range[2]={xmin,xmax};
      float y_range[2]={ymin,ymax};
      if(x_range[0]!= -999 && x_range[1]!=-999) h->GetXaxis()->SetRangeUser(x_range[0], x_range[1]);
      if(y_range[0]!= -999 && y_range[1]!=-999) h->GetYaxis()->SetRangeUser(y_range[0], y_range[1]);
      std::string xlabel_str(xLabel);
      std::string ylabel_str(yLabel);
      if(xlabel_str!="") h->GetXaxis()->SetTitle(xLabel);
      if(ylabel_str!="") h->GetYaxis()->SetTitle(yLabel);
      h->SetLineColor(1);
      h->SetTitle(h->GetName());
      h->SetStats(0);
      h->Draw("E");
      c->SaveAs(OutFileName_char);
     // c->Clear(); 
     // c->Close();
      delete c;
    }
  }
  //=============================== Overlay Histograms in each PDF ========================================================//
  else{
   //=============================== Declerations ========================================================//
    TCanvas *c = new TCanvas("c","c");
    THStack *stacked_histo = new THStack("hs","hs");
    auto* legend = new TLegend(0.8,0.67,0.95,0.88);
    c->cd();
    // Color of each histogram -- update for every one 
    int color;
    //Legend Entry of each Histogram -- update for every one
    const char* legend_entry;
    //===============================Histos to overlay loop ========================================================//
    for(int i = 0; i < histos_infile.size(); i++){
      //The histogram in file
      TH1F* h = (TH1F*) histos_infile.at(i);
      //Colour of histo
      if(stack_hColors.size()!=0 && stack_hColors.size()==histos_infile.size() ) color=stack_hColors.at(i);
      else{color=i+2;}
      //Lengend entry of histo
      if(legend_entries.size()!=0 && legend_entries.size()==histos_infile.size() ) legend_entry=legend_entries.at(i).c_str();
      else{legend_entry= "";}
            //Set the colour
      h->SetLineColor(color);
      //Scale to last entry if applicable 
      //h->Scale(histos_infile.at(histos_infile.size()-1)->Integral()/histos_in_region_vec.at(i)->Integral());
      //Add the histogram to overlay 
      stacked_histo->Add(h,"E");
      //Add Legend 
      legend->AddEntry(h,legend_entry, "l");
    }
    //===================================================================================================//
    //Draw Histogram 
    stacked_histo->Draw("nostack");
    //===============================Set Formatting ========================================================//
    float x_range[2]={xmin,xmax};
    float y_range[2]={ymin,ymax};
    if(x_range[0]!= -999 && x_range[1]!=-999) stacked_histo->GetXaxis()->SetRangeUser(x_range[0], x_range[1]);
    if(y_range[0]!= -999 && y_range[1]!=-999) stacked_histo->GetYaxis()->SetRangeUser(y_range[0], y_range[1]);
    std::string xlabel_str(xLabel);
    std::string ylabel_str(yLabel);
    if(xlabel_str!="") stacked_histo->GetXaxis()->SetTitle(xLabel);
    if(ylabel_str!="") stacked_histo->GetYaxis()->SetTitle(yLabel);
    stacked_histo->SetTitle(stacked_hname);
    //=====================================================================================================//
    //Draw Legend 
    legend->Draw("SAME");
    //===============================Write to File========================================================//
    std::string stacked_hist_name_str(stacked_hname);
    std::string file_name_str =stacked_hist_name_str+output_format; 
    const char* OutFileName_char = file_name_str.c_str();
    c->SaveAs(OutFileName_char);
    //=================================================================================================//
    //===============================Clean-up========================================================//
    c->Clear(); 
    c->Close();
    delete c;
    delete stacked_histo;
    delete legend;
  } 
}

std::vector<TH1F*> list_histos_inFile(std::string fname_str)
{

  char fname_char[fname_str.length()+1];
  strcpy(fname_char,fname_str.c_str());
  //Open the file 
  TFile f(fname_char,"READ");
  //Get an iterator which is a pointer to the list of object keys
  TIter next(f.GetListOfKeys());
  //Declare a key object
  TKey *key;
  //A vector to carry histogram object in the region 
  std::vector<TH1F*> v_histos_infile;
  //Iterate over the keys as long as next iterator is not null
  while ((key = (TKey*) next())) {
   //make a temporary histogram to carry the histo associated with each key 
   auto h = (TH1F*) key->ReadObj();
   //Set the histogram directory tto None so that histogram is not associated with file 
   h->SetDirectory(0);
   //push back the histo 
   v_histos_infile.push_back(h);
  }
  //Close the file --- if not closed here we see a memory leak as we iterate vectors! 
  f.Close();
   return v_histos_infile;
}