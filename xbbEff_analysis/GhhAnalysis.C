#define GhhAnalysis_cxx
#include "GhhAnalysis.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#ifndef __CINT__
#include "plotting_funcs.h"
#include "plotting_funcs.cpp"
#endif

void GhhAnalysis::Loop()
{

      if (fChain == 0) return;

      Long64_t nentries = fChain->GetEntriesFast();

      Long64_t nbytes = 0, nb = 0;


      TH1F *lrj_AllEta_h = new TH1F("lrj_AllEta_h", "lrj_AllEta_h", 20,0,2);
      TH1F *lrj_TaggedEta_h = new TH1F("lrj_TaggedEta_h", "lrj_TaggedEta_h", 20,0,2);
      TH1F *lrj_V2TaggedEta_h = new TH1F("lrj_V2TaggedEta_h", "lrj_V2TaggedEta_h", 20,0,2);
      TH1F *eff_h =  (TH1F*)lrj_AllEta_h->Clone("Ghhh_EffvEta_NoEtaReweight_AllTaggers");
      TH1F *effV2_h =  (TH1F*)lrj_AllEta_h->Clone("Ghhh_EffvEta_EtaReweight_AllTaggers");

      for (Long64_t jentry=0; jentry<nentries;jentry++) {
            
            Long64_t ientry = LoadTree(jentry);
            if (ientry < 0) break;
            nb = fChain->GetEntry(jentry);   nbytes += nb;

            //Sample topness 
            float ftop = 0.25; 
            //Initialize discriminant Dxbb
            float Dxbb = -99.0; //not sure what a default should be in case of nan      
            float Dxbb_V2 = -99.0; //not sure what a default should be in case of nan      
            
            //Choose truth Zbb jet using a dR method
            auto truth_lrj_itr = std::min_element(LargeRJetdR_Z->begin(), LargeRJetdR_Z->end());
            int truth_lrj = std::distance(LargeRJetdR_Z->begin(), truth_lrj_itr);
            //Kinematic Selection for valid leading jet 
            //if(LargeRJetMass->at(truth_lrj) < 50.0) continue;
            if(fabs(LargeRJetEta->at(truth_lrj)) > 2.0 ) continue;
            //if(fabs(LargeRJetPt->at(truth_lrj)) < 450.0 ) continue; 
            //NN Scores 
            float pHiggs = LargeRJetHbbScoreHiggs->at(truth_lrj);
            float pTop   = LargeRJetHbbScoreTop->at(truth_lrj);
            float pQCD   = LargeRJetHbbScoreQCD->at(truth_lrj);
            
            float pHiggs2 = LargeRJetHbbScoreHiggs_V2->at(truth_lrj);
            float pTop2   = LargeRJetHbbScoreTop_V2->at(truth_lrj);
            float pQCD2   = LargeRJetHbbScoreQCD_V2->at(truth_lrj);            
            //Calculate Dxbb discriminant 
            if(TMath::IsNaN(pHiggs) || TMath::IsNaN(pTop) || TMath::IsNaN(pQCD)) continue;
            Dxbb = log(pHiggs/(ftop*pTop+ (1-ftop)*pQCD));
            Dxbb_V2 = log(pHiggs2/(ftop*pTop2+ (1-ftop)*pQCD2));
            //Apply jet tagging WP cut for ftop=0.25
            bool jet_is_tagged_WP50 = (Dxbb > 3.13);
            bool jet_is_tagged_WP60 = (Dxbb > 2.55);
            bool jet_is_tagged_WP70 = (Dxbb > 1.92);
            bool jet_is_tagged_WP80 = (Dxbb > 1.20);

            bool jet_is_V2tagged_WP50 = (Dxbb_V2 > 3.13);
            bool jet_is_V2tagged_WP60 = (Dxbb_V2 > 2.55);
            bool jet_is_V2tagged_WP70 = (Dxbb_V2 > 1.92);
            bool jet_is_V2tagged_WP80 = (Dxbb_V2 > 1.20);

            //Fill Eta Histos 
            lrj_AllEta_h->Fill(fabs(LargeRJetEta->at(truth_lrj)));            
            if(jet_is_tagged_WP60) lrj_TaggedEta_h->Fill(fabs(LargeRJetEta->at(truth_lrj)));
            if(jet_is_V2tagged_WP60) lrj_V2TaggedEta_h->Fill(fabs(LargeRJetEta->at(truth_lrj)));
      }

      eff_h->Divide(lrj_TaggedEta_h,lrj_AllEta_h,1,1,"B");
      std::string fname = write_one_histo_to_file(eff_h);
      save_histos_from_file(fname,"|#eta|","#epsilon",0,2,0,1);
      effV2_h->Divide(lrj_V2TaggedEta_h,lrj_AllEta_h,1,1,"B");
      std::string fname1 = write_one_histo_to_file(effV2_h);
      save_histos_from_file(fname1,"|#eta|","#epsilon",0,2,0,1);

      std::vector<TH1F*> v_eff_V1vV2 ={eff_h,effV2_h};      
      std::vector<std::string> v_eff_V1vV2_legends = {"TaggerV1","TaggerV2"};

      std::string fname2 = write_Nhistos_to_file(v_eff_V1vV2,"Ghh_Eff_TaggerV1xTaggerV2");
      save_histos_from_file(fname2,"|#eta|","#epsilon",0,2,0,1,true,"Ghh_Eff_TaggerV1xTaggerV2",v_eff_V1vV2_legends);

}
//}
int run(){
  GhhAnalysis nominal;
  nominal.Loop();
  return 0;
}
